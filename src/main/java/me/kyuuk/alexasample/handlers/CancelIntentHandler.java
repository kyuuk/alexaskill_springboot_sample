package me.kyuuk.alexasample.handlers;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.Predicates;

import me.kyuuk.alexasample.utils.IntentNames;

@Component
public class CancelIntentHandler extends AbstractIntentHandler implements RequestHandler {
	private static final Logger logger = LogManager.getLogger(CancelIntentHandler.class);

	@Override
	public boolean canHandle(HandlerInput input) {
		return input.matches(Predicates.intentName(IntentNames.AMAZON_STOP_INTENT).or(Predicates.intentName(IntentNames.AMAZON_CANCEL_INTENT)));
	}
 
	@Override
	public Optional<Response> handle(HandlerInput input) {
		logger.info("Handeling leave/Cancel intent");
		return input.getResponseBuilder().withSpeech("GoodBye man!").withShouldEndSession(true).build();
	}

}
