package me.kyuuk.alexasample.handlers;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.Predicates;

public class LaunchRequestHandler implements RequestHandler {
	private static final Logger logger = LogManager.getLogger(LaunchRequestHandler.class);
	@Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(Predicates.requestType(LaunchRequest.class));
    }

	@Override
    public Optional<Response> handle(HandlerInput input) {
		logger.info("LaunchRequest");
        String speechText = "Here is a simple welcome speech or i can tell here what to do";
        return input.getResponseBuilder()
                .withSpeech(speechText).withSimpleCard("SpringBoot Sample", speechText)
				.withReprompt(speechText).withShouldEndSession(false).build();
	}

}
