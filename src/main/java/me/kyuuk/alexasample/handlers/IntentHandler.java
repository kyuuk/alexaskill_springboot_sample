package me.kyuuk.alexasample.handlers;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.Predicates;


@Component
public class IntentHandler extends AbstractIntentHandler implements RequestHandler {
	private static final Logger logger = LogManager.getLogger(IntentHandler.class);


	@Override
	public boolean canHandle(HandlerInput input) {
		return input.matches(Predicates.requestType(IntentRequest.class).and(Predicates.intentName("IntentName here")));
	}

	@Override
	public Optional<Response> handle(HandlerInput input) {
		logger.info("Start expert intent");
			String speech = "this is a sample speech that i'm telling you";
			return input.getResponseBuilder().withSpeech(speech).withShouldEndSession(true).build();
	}

}
