package me.kyuuk.alexasample.handlers;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.Predicates;

import me.kyuuk.alexasample.utils.IntentNames;

public class HelpIntentHandler implements RequestHandler {
	private static final Logger logger = LogManager.getLogger(CancelIntentHandler.class);
	@Override
	public boolean canHandle(HandlerInput input) {
		return input.matches(Predicates.intentName(IntentNames.AMAZON_HELP_INTENT));
	}

	@Override
	public Optional<Response> handle(HandlerInput input) {
		logger.info("Handeling Help intent intent");
		String helpSpeech = "here put a help speech for when user asks for help";
		return input.getResponseBuilder().withSpeech(helpSpeech )
				.withSimpleCard("SpringBoot Sample", helpSpeech).withShouldEndSession(false).build();
	}

}
