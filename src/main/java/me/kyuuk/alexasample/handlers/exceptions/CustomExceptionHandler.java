package me.kyuuk.alexasample.handlers.exceptions;

import java.util.Optional;

import com.amazon.ask.dispatcher.exception.ExceptionHandler;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;

public class CustomExceptionHandler implements ExceptionHandler{

	@Override
	public boolean canHandle(HandlerInput input, Throwable throwable) {
		return true;
	}

	@Override
	public Optional<Response> handle(HandlerInput input, Throwable throwable) {
		return input.getResponseBuilder().withSpeech("Une erreur est survenue").withShouldEndSession(true).build();
	}

}
