package me.kyuuk.alexasample.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.interfaces.system.SystemState;
import com.amazon.ask.model.services.directive.DirectiveServiceClient;
import com.amazon.ask.model.services.directive.Header;
import com.amazon.ask.model.services.directive.SendDirectiveRequest;
import com.amazon.ask.model.services.directive.SpeakDirective;


public abstract class AbstractIntentHandler {
	private static final Logger logger = LogManager.getLogger(AbstractIntentHandler.class);
	
	protected static void sendProgressiveResponse(HandlerInput input) {
		DirectiveServiceClient apiCli = input.getServiceClientFactory().getDirectiveService();
		SystemState system = input.getRequestEnvelope().getContext().getSystem();
		logger.debug("sending progressive response to [{}] with token [{}]", system.getApiEndpoint(),
				system.getApiAccessToken());
		SendDirectiveRequest.Builder directiveBuilder = SendDirectiveRequest.builder();
		directiveBuilder.withHeader(Header.builder().withRequestId(input.getRequest().getRequestId()).build());
		apiCli.enqueue(directiveBuilder
				.withDirective(SpeakDirective.builder().withSpeech("i'm working on it").build()).build());
		logger.info("Progressive response sent while processing long response");
	}
}
