/**
 * 
 */
package me.kyuuk.alexasample.config.spring;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazon.ask.Skill;
import com.amazon.ask.Skills;
import com.amazon.ask.builder.StandardSkillBuilder;
import com.amazon.ask.servlet.SkillServlet;

import me.kyuuk.alexasample.handlers.CancelIntentHandler;
import me.kyuuk.alexasample.handlers.HelpIntentHandler;
import me.kyuuk.alexasample.handlers.IntentHandler;
import me.kyuuk.alexasample.handlers.LaunchRequestHandler;
import me.kyuuk.alexasample.handlers.SessionEndedRequestHandler;
import me.kyuuk.alexasample.handlers.exceptions.CustomExceptionHandler;


/**
 * @author kyuk
 *
 */
@Configuration
public class SpringAppConfig {

	@Bean
	public ServletRegistrationBean<SkillServlet> registerSpeechletServlet() {
		StandardSkillBuilder builder = Skills.standard();
		builder
				.addRequestHandler(new LaunchRequestHandler())
				.addRequestHandler(new IntentHandler())
				.addRequestHandler(new SessionEndedRequestHandler())
				.addRequestHandler(new CancelIntentHandler())
				.addRequestHandler(new HelpIntentHandler())
				.addExceptionHandler(new CustomExceptionHandler());
		Skill skill = builder.build();

		SkillServlet speechletServlet = new SkillServlet(skill);

		return new ServletRegistrationBean<>(speechletServlet, "/alexa");
	}

}
