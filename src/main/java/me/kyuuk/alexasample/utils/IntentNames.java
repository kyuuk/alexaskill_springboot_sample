package me.kyuuk.alexasample.utils;

public class IntentNames {

	/**
	 * Alexa built-in intents names
	 */
	public static final String AMAZON_HELP_INTENT= "AMAZON.HelpIntent";
	public static final String AMAZON_CANCEL_INTENT= "AMAZON.CancelIntent";
	public static final String AMAZON_STOP_INTENT= "AMAZON.StopIntent";
	
	/**
	 * Custom Intents names
	 */
	public static final String CUSTOM_INTENT_NAME= "Custom_IntentName_HERE";
}
