/**
 * 
 */
package me.kyuuk.alexasample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author kyuk
 *
 */
@SpringBootApplication
public class AlexaSample
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		SpringApplication.run(AlexaSample.class, args);
	}

}
